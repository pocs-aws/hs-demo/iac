locals {
  sqs  = {
    name = "hs_queue"
    visibility_timeout = 120
    polling = 15
    enabled_dlq = true
    dlq_visibility_timeout = 0
    dlq_message_retention = 1209600
    dlq_delay = 1
    dlq_polling = 15
  }

  sns = {
    name = "hs_sns_generar_broma_o_saludar"
  }

  role_to_publish_sns = {
    name = "role_hs_publish_to_topic"
  }
}