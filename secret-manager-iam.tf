locals {
  execution_roles = [
    aws_iam_role.iam_for_lambda_execution_hs.arn
  ]
}

data "aws_iam_policy_document" "policy_lambdas_assume_role_allowed" {
  statement {
    actions = [
      "sts:AssumeRole"]

    principals {
      type = "AWS"
      identifiers = local.execution_roles
    }
  }
}

resource "aws_iam_policy" "policy_get_from_secret_hs" {
  name = "policy_get_from_secret_hs"
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "secretsmanager:GetSecretValue",
        ]
        Effect = "Allow"
        Resource = aws_secretsmanager_secret.secret_manager_hs.arn
      },
      {
        Sid = "AllowUseOfTheKey",
        Effect = "Allow",
        Action = [
          "kms:Decrypt",
          "kms:DescribeKey"
        ],
        Resource = "*",
        Condition = {
          "StringEquals" = {
            "kms:ViaService" = "secretsmanager.${data.aws_region.current.name}.amazonaws.com"
          },
          "StringLike" = {
            "kms:EncryptionContext:SecretARN" = aws_secretsmanager_secret.secret_manager_hs.id
          }
        }
      }
    ]
  })
}

resource "aws_iam_role" "role_get_secret_hs" {
  name = "role_get_secret_hs"
  assume_role_policy = data.aws_iam_policy_document.policy_lambdas_assume_role_allowed.json
  managed_policy_arns = [
    aws_iam_policy.policy_get_from_secret_hs.arn]
  tags = merge(
  local.tf_tags,
  {
    Name = "role_get_secret_hs"
  }
  )
}
