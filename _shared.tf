locals {
  tf_tags = {
    Terraform = "yes"
    Environment = "poc"
    Project = "Demo Hacker Space 2022"
  }
  prefix_project = "hs"
  vpc_name = "vpc-${local.prefix_project}"
}
