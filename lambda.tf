provider "archive" {}

resource "aws_lambda_function" "lambda" {
  function_name = "lambda"
  filename         = local.lambda_local_zip
  role    = aws_iam_role.iam_for_lambda_execution_hs.arn
  handler = "lambda.handler"
  runtime = "nodejs14.x"
  memory_size = 512
  environment {
    variables = {
      API_JOKES_ENDPOINT = local.apis.jokes.endpoint
      API_JOKES_HEADER_HOST = local.apis.jokes.header_host
      ROLE_GET_SECRET = aws_iam_role.role_get_secret_hs.arn
      SECRET_ARN = aws_secretsmanager_secret.secret_manager_hs.arn
      REGION =  data.aws_region.current.name
    }
  }

  vpc_config {
    # Every subnet should be able to reach an EFS mount target in the same Availability Zone. Cross-AZ mounts are not permitted.
    subnet_ids         = [element(data.aws_subnets.publics.ids, 0)]
    security_group_ids = [aws_security_group.egress_all.id]
  }
}


resource "aws_cloudwatch_log_group" "cloudwacth_hs" {
  name              = "/aws/lambda/${aws_lambda_function.lambda.function_name}"
  retention_in_days = 7
}
