data "aws_iam_policy_document" "policy_lambdas_assume_role_hs" {
  statement {
    sid    = "1"
    effect = "Allow"
    principals {
      identifiers = ["lambda.amazonaws.com"]
      type        = "Service"
    }
    actions = ["sts:AssumeRole"]
  }
}

# See also the following AWS managed policy: AWSLambdaBasicExecutionRole
resource "aws_iam_policy" "lambda_logging_hs" {
  name        = "lambda_logging_hs"
  path        = "/"
  description = "IAM policy for logging from a lambda"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Sid = "LoggingLambda",
        Action = [
          "logs:CreateLogGroup",
          "logs:CreateLogStream",
          "logs:PutLogEvents"
        ]
        Effect = "Allow"
        Resource = "arn:aws:logs:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:log-group:${aws_cloudwatch_log_group.cloudwacth_hs.name}:*"
      }
    ]
  })
}

# See also the following AWS managed policy: AWSLambdaVPCAccessExecutionRole
resource "aws_iam_policy" "lambda_vpc_execution_hs" {
  name        = "lambda_vpc_execution_hs"
  path        = "/"
  description = "IAM policy for logging from a lambda"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Sid = "VPCExecutionLambda",
        Action = [
          "ec2:CreateNetworkInterface",
          "ec2:DescribeNetworkInterfaces",
          "ec2:DeleteNetworkInterface",
          "ec2:AssignPrivateIpAddresses",
          "ec2:UnassignPrivateIpAddresses"
        ]
        Effect = "Allow"
        Resource = "*"
      }
    ]
  })
}

resource "aws_iam_role" "iam_for_lambda_execution_hs" {
  name               = "iam_for_lambda_execution_hs"
  assume_role_policy = data.aws_iam_policy_document.policy_lambdas_assume_role_hs.json
}

resource "aws_iam_role_policy_attachment" "policy_attachment_lambda_logs" {
  role       = aws_iam_role.iam_for_lambda_execution_hs.name
  policy_arn = aws_iam_policy.lambda_logging_hs.arn
}


resource "aws_iam_role_policy_attachment" "policy_attachment_lambda_vpc_execution_hs" {
  role       = aws_iam_role.iam_for_lambda_execution_hs.name
  policy_arn = aws_iam_policy.lambda_vpc_execution_hs.arn
}

resource "aws_iam_role_policy_attachment" "policy_attachment_lambda_sqs_execution_hs" {
  role       = aws_iam_role.iam_for_lambda_execution_hs.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaSQSQueueExecutionRole"
}