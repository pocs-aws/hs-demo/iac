resource "aws_security_group" "egress_all" {
  name        = "egress-all"
  tags = merge(local.tf_tags, {
    Name = "egress-all"
  })
  description = "Allow all outbound traffic"
  vpc_id      = data.aws_vpc.vpc_hs.id
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}