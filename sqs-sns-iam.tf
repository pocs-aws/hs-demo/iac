resource "aws_iam_policy" "policy_hs_publish_to_topics" {
  name = "hs_publish_to_topics"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Sid = "PublishSns"
        Action = [
          "sns:Publish",
        ]
        Effect = "Allow"
        Resource = aws_sns_topic.hs_sns_generar_broma_o_saludar.arn
      }
    ]
  })
}

resource "aws_iam_role" "role_hs_publish_to_topic" {
  name = local.role_to_publish_sns.name
  assume_role_policy = data.aws_iam_policy_document.policy_lambdas_assume_role_allowed.json
  managed_policy_arns = [
    aws_iam_policy.policy_hs_publish_to_topics.arn]
  tags = local.tf_tags
}