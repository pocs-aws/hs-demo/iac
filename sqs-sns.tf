
resource "aws_sns_topic" "hs_sns_generar_broma_o_saludar" {
  name = local.sns.name
  policy = <<POLICY
  {
    "Version": "2008-10-17",
    "Id": "1",
    "Statement": [
      {
        "Sid": "PublishOnSNSFromRole",
        "Effect": "Allow",
        "Principal": { "AWS": "*" },
        "Action": "SNS:Publish",
        "Resource": "arn:aws:sns:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:${local.sns.name}",
        "Condition": {
          "ArnEquals": {
            "aws:SourceArn": "arn:aws:iam:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:role/${local.role_to_publish_sns.name}"
          }
        }
      }
    ]
  }
  POLICY
}

resource "aws_sqs_queue" "hs_queue_generar_broma" {
  name                       = "hs_queue_generar_broma"
  receive_wait_time_seconds  = 20
  message_retention_seconds  = 18400

}

resource "aws_sqs_queue" "hs_queue_saludar" {
  name                       = "hs_queue_saludar"
  receive_wait_time_seconds  = 20
  message_retention_seconds  = 18400
}

resource "aws_sqs_queue_policy" "hs_queue_policy_generar_broma" {
  queue_url = aws_sqs_queue.hs_queue_generar_broma.id
  policy    = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "sns.amazonaws.com"
      },
      "Action": [
                  "sqs:SendMessage",
                  "sqs:GetQueueAttributes",
                  "sqs:ReceiveMessage"
                ],
      "Resource": [
        "${aws_sqs_queue.hs_queue_generar_broma.arn}"
      ],
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": "${aws_sns_topic.hs_sns_generar_broma_o_saludar.arn}"
        }
      }
    }
  ]
}
EOF
}

resource "aws_sqs_queue_policy" "hs_queue_policy_saludar" {
  queue_url = aws_sqs_queue.hs_queue_saludar.id
  policy    = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "sns.amazonaws.com"
      },
      "Action": [
                  "sqs:SendMessage",
                  "sqs:GetQueueAttributes",
                  "sqs:ReceiveMessage"
                ],
      "Resource": [
        "${aws_sqs_queue.hs_queue_saludar.arn}"
      ],
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": "${aws_sns_topic.hs_sns_generar_broma_o_saludar.arn}"
        }
      }
    }
  ]
}
EOF
}

resource "aws_sns_topic_subscription" "hs_subscription_sns_to_sqs_generar_broma" {
  topic_arn = aws_sns_topic.hs_sns_generar_broma_o_saludar.arn
  protocol = "sqs"
  endpoint = aws_sqs_queue.hs_queue_generar_broma.arn
}

resource "aws_sns_topic_subscription" "hs_subscription_sns_to_sqs_saludar" {
  topic_arn = aws_sns_topic.hs_sns_generar_broma_o_saludar.arn
  protocol = "sqs"
  endpoint = aws_sqs_queue.hs_queue_saludar.arn
}

resource "aws_lambda_event_source_mapping" "hs_sqs_event_mapping" {
  event_source_arn                   = aws_sqs_queue.hs_queue_generar_broma.arn
  function_name                      = aws_lambda_function.lambda.function_name
  batch_size                         = 1
  maximum_batching_window_in_seconds = 1
}