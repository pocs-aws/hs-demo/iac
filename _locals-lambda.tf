locals {
  lambda_local_zip = "../hs-lambda/dist/lambda.zip"
  apis = {
    jokes = {
      endpoint = "https://joke-generator.p.rapidapi.com"
      header_host = "joke-generator.p.rapidapi.com"
    }
  }
}