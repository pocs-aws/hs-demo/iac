variable "api_jokes_key" {}

locals {
  secrets = {
    apis = {
      apiJokesKey = var.api_jokes_key
    }
  }
}

resource "aws_kms_key" "kms_key_hs" {
  description = "KMS for Hacker Space"
  deletion_window_in_days = 7
  tags = merge(
  local.tf_tags,
  {
    "Name" = "kms-hs"
    "nombre" = "kms-hs"
    "plataforma" = "KMS"
  })
}

#Creación del secret
resource "aws_secretsmanager_secret" "secret_manager_hs" {
  name = "secret-hs-2022"
  description = "Secret for Hacker Space"
  tags = merge(
  local.tf_tags,
  {
    "Name" = "secret-hs-2022"
    "plataforma" = "SecretsManager"
  })
  kms_key_id = aws_kms_key.kms_key_hs.arn
}

#Insertar el objeto que creamos y mantener versionado  a modo de que no sea manipulado desde la consola
resource "aws_secretsmanager_secret_version" "secret_manager_hs_version" {
  depends_on = [
    aws_secretsmanager_secret.secret_manager_hs]
  secret_id = aws_secretsmanager_secret.secret_manager_hs.id
  secret_string = jsonencode(local.secrets)
  version_stages = [
    "AWSCURRENT"]
}
