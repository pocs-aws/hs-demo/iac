data "aws_vpc" "vpc_hs" {
  filter {
    name   = "tag:Name"
    values = [
      local.vpc_name]
  }
}

data "aws_subnets" "publics" {
  filter {
    name   = "tag:Name"
    values = ["${local.vpc_name}-public-*"]
  }
}

data "aws_internet_gateway" "igw" {
  filter {
    name   = "tag:Name"
    values = ["igw-${local.vpc_name}"]
  }
}

data "aws_route_table" "rtb_public" {
  filter {
    name   = "tag:Name"
    values = ["rtb-${local.vpc_name}-public"]
  }
}